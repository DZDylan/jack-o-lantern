#include <stdio.h>
#include <stdlib.h>
#include "MyFunctions.h"
#include "HelperFunctions.c"

/*
 * This is C program that creates a purple circle as ppm. 
 * Usage: 
 *
 * PurpleCircle OutFileName numRows numCols radius
 */ 

int main(int argc,    // This is the number of things that get passed into this function.
         char *argv[] // This is the array of things passed into this function. 
        ){

    // Add here the variable declarations. 
    int numRows; 
    int numCols;
    int imageSize;
    int row, col;
    int radius;
    int InOut;           // Flag where (0 = out of the circle, 1 = in the circle)
    unsigned char *outImage;  /* Pixel Pointer  */ 
    unsigned char *ptr;       /* moving pointer */
    FILE *outputFP;           /* Out put file pointer */  

    printf("====================================================\n");
    printf("I'm making a plain pixel map of a purple cicle.     \n");
    printf("====================================================\n");

    if(argc != 5){
        printf("Usage : ./PurpleCircle OUTFileName numRows numCols radius \n");
        exit(1);
    }
    if( ( numRows = atoi(argv[2]) ) <= 0){  // <---------- Moved a ")" Here to fix code. And below too.
         printf("Error : numRows needs to be positive!\n");
    }
    if( ( numCols = atoi(argv[3]) ) <= 0){
         printf("Error : numCols needs to be positive!\n"); 
    }
    if( ( radius = atoi(argv[4]) ) <= 0){
         printf("What are you thinking! A negative value for a circles's radius? ");
    }

    printf("Values of numRows = %d , numCols = %d, radius = %d \n", numRows, numCols, radius);

    // =======================================================
    // Set up space for the pixel map. 
    // =======================================================

    imageSize = numRows*numCols*3; 
    outImage = (unsigned char *) malloc(imageSize); // space for the image. 

    if ((outputFP = fopen(argv[1], "w")) == NULL){
       perror("output opening error!");
       printf("Error could not open the output file \n");
       exit(1);
    }

    ptr = outImage; // Puts the pointer for our pixel map at its starting point in the memory. 

    for(row = 0; row < numRows; row++){
        for(col = 0; col < numCols; col++){
            // Is this pixel inside the circle? 
            InOut = InCircle(numRows, numCols, radius, row, col);

            if(InOut == 1){
                // Orange Pixel
                *ptr     = 255;
                *(ptr+1) = 165;
                *(ptr+2) =   0; 
            } else {
                // White Pixel
                *ptr     = 255;
                *(ptr+1) = 255; 
                *(ptr+2) = 255;
            }
		// creates a stem  
	    if(row > 100 &&  row < 255) {
                if(col > 475  && col < 525) {
		    *ptr      =   0;
                    *(ptr+1)  = 255;
                    *(ptr+2)  =   0;
                }
            }
		// Creating the eyebrows
	    if(row > 360 && row < 370) {
               if(col > 340 && col < 410 || col > 590 && col < 660){
                    *ptr      = 0;
                    *(ptr+1)  = 0;
                    *(ptr+2)  = 0;
                }
            }
                // creates the eyes
	    if(row > 400 && row < 450){
               if(col > 350 && col < 400){
                    *ptr      = 0;
                    *(ptr+1)  = 0;
                    *(ptr+2)  = 0;
               }
               if(col > 600 && col < 650){
                    *ptr      = 0;
                    *(ptr+1)  = 0;
                    *(ptr+2)  = 0;
              }
            }
		// Creating the smile
            if(row > 525 && row < 550){
               if(col > 400 && col < 425 || col > 575 && col < 600){
                    *ptr      = 0;
                    *(ptr+1)  = 0;
                    *(ptr+2)  = 0;
               }
            }
            if(row > 550 && row < 575){
               if(col > 425 && col < 450 || col > 550 && col < 575){
                    *ptr      = 0;
                    *(ptr+1)  = 0;
                    *(ptr+2)  = 0;
              }
            }
            if(row > 575 && row < 600){
               if(col > 450 && col < 475 || col > 525 && col < 550){
                    *ptr      = 0;
                    *(ptr+1)  = 0;
                    *(ptr+2)  = 0;
               }
            }
            if(row > 600 && row < 625){
               if(col > 475 && col < 525){
                    *ptr      = 0;
                    *(ptr+1)  = 0;
                    *(ptr+2)  = 0;
               }
            }
	         // Creating a nose
            if(row > 500 && row < 505){ 
               if(col > 495 && col < 505) {
                    *ptr     = 0;
                    *(ptr+1) = 0;
                    *(ptr+2) = 0;
               }
            }
            if(row > 505 && row < 510){
               if(col > 505 && col < 515  ||  col < 495 && col > 485){
                  *ptr       = 0;
                  *(ptr+1)   = 0;
                  *(ptr+2)   = 0;
               }
            }
 
            // Advance the pointer
            ptr += 3; 
        }
    }

    // Put it all together into a single image. 
    fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
    fwrite(outImage, 1, imageSize, outputFP);

    /* Done */ 
    fclose(outputFP); 

    return 0; 
}

